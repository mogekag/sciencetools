# Import for scientific calulations
import PyGuide
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pylab
import math

from astropy.io import fits
from scipy import signal

# Import to handle sleep times and random numbers
import sys
import time

def getData(filename):
	hdu_list = fits.open(filename)
	image_data = hdu_list[0].data
	hdu_list.close()
	
	# If this is used, the image will be plotted
        #plt.imshow(image_data, cmap='gray')
        #plt.colorbar()
        #pylab.show()

	return image_data

def createSaturationMask(image_data, x, y, saturationlevel):
	mask = [[0 for p in range(y)] for q in range(x)]
	for i in range(0,x):
		for j in range(0,y):
			if(image_data[i][j] == saturationlevel):
				mask[i][j] = 1
	return mask


def main():

	# Quick Guide
	# [+] means message from the system
	# [-] means that user's action is required
	# [=] default values

	# Open the image file and save the pixel intensity into an 2D numpy array
	filename = raw_input("[-] Input the FITS/FIT image filename: ")
	image_data = getData(filename)	

	# Define the length X and Y of the image
	y = len(image_data[0])
	x = image_data.size/y

	# CCD Info
	# There are other configuration values that may be required
	print("[+] Do you want to input the CCD info?\n")
	print("[+] Default Values:")
	print("[=]\tBias = 0 [ADU]")
	print("[=]\tReadnoise = 9.3 [e-]")
	print("[=]\tGain = 0.37 [e-/ADU]")
	print("[=]\tSaturation Level = 65535 [ADU]")
	print("[=]\tThreshold = 3 [ADU]")
	print("[=]\tRadius = None")
	
	infOption = raw_input('\n[-] (Y)es/(N)o: ')
	if(infOption == 'Y'):
		bias = raw_input('[-] Bias [ADU]: ')
		readnoise = raw_input('[-] Readnoise [e-]: ')
		ccdgain = raw_input('[-] CCD Gain [e-/ADU]: ')
		saturationlevel = raw_input('[-] Saturation Level [ADU]: ')
		thresh = raw_input('[-] Threshold [ADU]: ')
		radMult = 1
		rad = None
		verbosity = 0
		doDS9 = 0

		print('\n[+] Updating CCD Info')
	else:
		bias = 0
		readnoise = 9.3
		ccdgain = 0.37
		saturationlevel = 65535
		thresh = 3
		radMult = 1
		rad = None
		verbosity = 0
		doDS9 = 0

	# Creates a mask of saturated values
	print("[+] Creating saturation mask array.")
	saturationMask = createSaturationMask(image_data, x, y, saturationlevel)

	# Valid data mask
	# Prompt if the user want to use all the data!!!!
	print("[+] Creating valid data mask.")
	validMask = [[0 for p in range(0,y)] for q in range(0,x)]


	# Instanciate CCDInfo object
	CCDInfo = PyGuide.CCDInfo(bias, readnoise, ccdgain, saturationlevel)

	print("\n[+] Looking for star's centroids")
	
	# Find Stars with PyGuide
	centroidData = PyGuide.findStars(image_data, validMask, saturationMask, CCDInfo, thresh, radMult, rad, verbosity, doDS9)
        outputFilename = raw_input("[-] Type the output filename: ");
        output = open(outputFilename,'w')

	centroidSize = len(centroidData[0]) - 1
	#centroids = np.zeros(shape=(y,x))
	for i in range(0, centroidSize):
		output.write('{} {}\n'.format(math.floor(centroidData[0][i].xyCtr[0]), math.floor(centroidData[0][i].xyCtr[1])))
	#	xCentroid = math.ceil(centroidData[0][i].xyCtr[0])
	#	yCentroid = math.ceil(centroidData[0][i].xyCtr[1])
	#	centroids[xCentroid][yCentroid] = 1
	output.close()
if __name__ == "__main__": main()
