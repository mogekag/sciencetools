import numpy as np
import math
import matplotlib
import matplotlib.pyplot as plt
import pylab

from astropy.io import fits

# Open a FIT/FITS image file and return it as a numpy 2D-array
def openImage(filename):
    hdu_list = fits.open(filename)
    image_data = hdu_list[0].data
    hdu_list.close()
    
    return image_data

# Count the number of lines in a file and return an integer number
def countLines(filename):
    counter = 0
    f = open(filename, "r")
    for line in f.read().split('\n'):
        counter += 1
    f.close()

    return int(counter - 1)

# Get the sky value for a frame
def getSkyValue(image_data, x1, y1, x2, y2, radius):
    counts = 0
    for i in range(x1, x2):
        for j in range(y1, y2):
            counts += image_data[i][j]
    area = (x2-x1)*(y2-y1)
    average = (np.pi*radius*radius)*(counts/area)

    return average

# Get the average counts on a radius for a set of coordinates
def getAverageCounts(image_data, x, y, radius):
    pixelNumber = 0
    countSum = 0
    mradius = (-1)*radius
    for i in range(mradius, radius):
        for j in range(mradius, radius):
            aux1 = int(x)+i
            aux2 = int(y)+j
            countSum += image_data[aux2][aux1]
            pixelNumber += 1

    # pi/4 is the ratio of the area of a circle with the area of a square, to get closer results to the circular apperture from MaxinDL
    return (np.pi/4)*countSum

# Calculate the magzero for a specific frame
def calculateMagzero(countsMinusSky, magnitude):
    length = len(countsMinusSky)
    magzero = np.zeros(length)
    for i in range(0,length):
        magzero[i] = (2.5*math.log((0.37*countsMinusSky[i]),10))+magnitude[i]

    return magzero

def magzero(firstFrame, firstInput, secondInput, thirdInput, radius, x1Box, y1Box, x2Box, y2Box):

    linesInput1 = countLines(firstInput)
    linesInput2 = countLines(secondInput)
    linesInput3 = countLines(thirdInput)

    frameName = []
    frameName.append(firstFrame)
    magnitude = np.zeros(linesInput1)
    coordinates = np.zeros(shape=(int(linesInput2+1),2,linesInput1))
    starsCoordinate = np.zeros(shape=(linesInput1,2,linesInput3))
    xDifference = np.zeros(linesInput2+1)
    yDifference = np.zeros(linesInput2+1)

    # Read input file 1
    input1 = open(firstInput,'r')
    for i in range(0,linesInput1):
        line = input1.readline()
        line = line.split()
        coordinates[0][0][i] = line[0]
        coordinates[0][1][i] = line[1]
        magnitude[i] = line[2]
    input1.close()

    # Read input file 2
    input2 = open(secondInput,'r')
    for i in range(1,linesInput2+1):
        line2 = input2.readline()
        line2 = line2.split()
        frameName.append(line2[0])
        coordinates[i][0][0] = line2[1]
        coordinates[i][1][0] = line2[2]
    input2.close()

    # Red input file 3
    input3 = open(thirdInput,'r')
    for i in range(0,linesInput3):
        line3 = input3.readline()
        line3 = line3.split()
        starsCoordinate[0][0][i] = line3[0]
        starsCoordinate[0][1][i] = line3[1]
    input3.close()

    # Calculate difference on coordinates from one frame to another
    xDifference[0] = 0
    yDifference[0] = 0
    for i in range(1,linesInput2+1):
        xDifference[i] = coordinates[0][0][0] - coordinates[i][0][0]
        yDifference[i] = coordinates[0][1][0] - coordinates[i][1][0]
        for j in range(1,linesInput1):
            coordinates[i][0][j] = coordinates[0][0][j] - xDifference[i]
            coordinates[i][1][j] = coordinates[0][1][j] - yDifference[i]
	for j in range(1, linesInput3):
            starsCoordinate[i][0][j] = starsCoordinate[0][0][j] - xDifference[i]
            starsCoordinate[i][1][j] = starsCoordinate[0][1][j] - yDifference[i]


    # From this point, we already have the names of files in one array and
    # the coordinates of every file in a 3D-array

    # Let's calculate the average counts minus the sky background for every standard stars
    backgroundSky = np.zeros(linesInput2+1)
    averageMinusSky = np.zeros(shape=((linesInput2+1),linesInput1))
    magzero = np.zeros(shape=((linesInput2+1),linesInput1))
    magzeroMean = np.zeros(linesInput2+1)
    magzeroSTDEV = np.zeros(linesInput2+1)

    for i in range(0,(linesInput2+1)):
        image_data = openImage(frameName[i])
        backgroundSky[i] = getSkyValue(image_data, x1Box, y1Box, x2Box, y2Box, radius)
        for j in range(0, linesInput1):
            averageMinusSky[i][j] = getAverageCounts(image_data, coordinates[i][0][j], coordinates[i][1][j], radius) - backgroundSky[i]
        magzero[i] = calculateMagzero(averageMinusSky[i], magnitude)
        magzeroMean[i] = np.mean(magzero[i])
        magzeroSTDEV[i] = np.std(magzero[i])

    return frameName, magzeroMean, magzeroSTDEV, starsCoordinate, xDifference, yDifference

def calculateMagnitude(filename, magzeroMean, starsCoordinate, radius, x1Box, y1Box, x2Box, y2Box, xDifference, yDifference):
    numberOfStars = len(starsCoordinate[0][0])
    countsMinusSky = np.zeros(shape=(len(filename),numberOfStars))
    magnitude = np.zeros(shape=(len(filename),numberOfStars))
    gain = 0.37
    for i in range(0,len(filename)):
        image_data = openImage(filename[i])
        backgroundSky = getSkyValue(image_data, int(x1Box+xDifference[i]), int(y1Box+yDifference[i]), int(x2Box+xDifference[i]), int(y2Box+yDifference[i]), radius)
        for j in range(0,numberOfStars):
            countsMinusSky[i][j] = getAverageCounts(image_data, starsCoordinate[i][0][j], starsCoordinate[i][1][j], radius) - (0*backgroundSky)
            magnitude[i][j] = magzeroMean[i] - (2.5)*math.log(gain*countsMinusSky[i][j],10)
    return magnitude

def main():
    
    firstFrame = raw_input("[+] Input the first frame filename: ")
    firstInput = raw_input("[+] Input the filename of the first input file: ")
    secondInput = raw_input("[+] Input the filename of the second input file: ")
    radius = int(raw_input("[+] Input the FWHM of the first frame to use as radius: "))

    print("[+] In order to calculate the background sky value, inform the box coordinates.")
    x1Box = int(raw_input("[-] Initial X Coordinate: "))
    y1Box = int(raw_input("[-] Initial Y Coordinate: "))
    x2Box = int(raw_input("[-] Final X Coordinate: "))
    y2Box = int(raw_input("[-] Final Y Coordinate: "))

    # filename: 1D-array that contain the name of the files analized
    # magzeroMean: 1D-array that contain the mean of the magzero values
    # magzeroSTDEV: 1D-array that contain the standard deviation of the magzero values
    
    print("[+] Calculation magzero...")
    thirdInput = raw_input("[+] Input the filename of the input file containing the stars that you want to calculate tha magnitude: ")
    
    filename, magzeroMean, magzeroSTDEV, starsCoordinate, xDifference, yDifference = magzero(firstFrame, firstInput, secondInput, thirdInput, radius, x1Box, y1Box, x2Box, y2Box)
    magnitude = calculateMagnitude(filename, magzeroMean, starsCoordinate, radius, x1Box, y1Box, x2Box, y2Box, xDifference, yDifference)
    print("Magzero:")
    print magzeroMean
    print("Magzero STDEV:")
    print magzeroSTDEV
    print("Magnitudes for frame:") 
    print magnitude

if __name__ == "__main__": main()
