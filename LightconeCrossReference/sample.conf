# ARGS	VALUE
######################## Illustris Lightcone Matching Algorithm Configuration File ########################
#													  #
#			- Please keep this header in future configuration files.			  #
#			- This will help people identify the variables and arguments.			  #
#			- Keep the first line of this file, it works as the label of the columns.	  #
#													  #
#													  #
#	OUTPUT1		Name/Path of the output file to hold the information of the projected objects.    #
#			This file will contain the centroid of the objects within the field of view and   #
#			intrinsic information from the simulation (i.e. mass, redshift, magnitude, etc).  #		
#			DEFAULT: output-coordinates.txt							  #
#													  #
#													  #
#	OUTPUT2		Name/Path of the output file to hold the information of matched objects.	  #
#			This files look similar to OUTPUT1, but it will contain a cross match of	  #
#			of informations between the source extractor analysis and the simulation.	  #
#			DEFAULT: output-matched.txt							  #
#													  #
#	OUTPUT3		Name/Path of the output file to hold the non-matched objects.			  #
#			This file will have a list of X and Y coordinates of those objects whose 	  #
#			centroid couldn't be matched. (Usually pixels from high noise background)	  #
#			DEFAULT: output-nonmatched.txt							  #
#													  #
#	SEX_CAT		Name/Path of the Source Extractor's output catalog.				  #
#			For better results consider using the hot-cold merged catalog.			  #
#			DEFAULT: none - FIELD REQUIRED							  #
#													  #
#	HUDF_CAT	Name/Path of the file containing a summary of the snapshots.			  #
#			This file contains information about the lightcone geometry and the positioning   #
#			of the simulation cube replications.						  #
#			DEFAULT: none - FIELD REQUIRED							  #
#													  #
#	SUBHALO_CAT	PATH for the folder containing the subhalo catalogs.				  #
#			Subhalo catalogs contain the information (position and intrinsic properties)      #
#			of objects within the cylinder volume cropped from a give snapshot.		  #
#													  #
#			The files need to be named: cylinder_XXXX_subhalo_catalog.txt			  #
#			Where XXXX is a 4 digits (NECESSARILY) ID number. (e.g. 0002 or 0095)		  #
#			DEFAULT: none - FIELD REQUIRED							  #
#												  	  #
#	CYLINDER_CAT	PATH for the folder containing the cylinder information.			  #
#			The cylinder informations will be store in the header of a .FITS file and 	  #
#			there will be one file for each subhalo catalog.				  #
#			DEFAULT: none - FIELD REQUIRED							  #
#													  #
#			The files need to be named: new_noimage_XXXX.fits				  #
#			Where XXXX is a 4 digits (NECESSARILY) ID number. (e.g. 0002 or 0095)		  #
#													  #
#	SNAPSHOT_MIN	Number of the first snapshot to be considered					  #
#			DEFAULT: 02									  #
#													  #
#	SNAPSHOT_MAX	Number of the last snapshot to be considered					  #
#			DEFAULT: 95									  #
#													  # 
#	X_ANGULAR_FOV	Angular field of view in the X direction [radians]				  # 
#			DEFAUL: 0.0008264462								  # 
#													  # 
#	Y_ANGULAR_FOV	Angular field of view in the Y direction [radians]				  # 
#			DEFAULT: 0008264462								  # 
#													  # 
#	X_CENTER	X coordinate of the center of the image [pixels]				  # 
#			DEFAULT: 2048									  # 
#													  # 
#	Y_CENTER	Y coordinate of the center of the image [pixels]				  # 
#			DEFAULT: 2048									  # 
#													  # 
###########################################################################################################
OUTPUT1		./output/FIELDC_SB7_coordinates.txt 
OUTPUT2		./output/FIELDC_SB27_matched.txt
OUTPUT3		./output/FIELDC_SB27_notMatched.txt
SEX_CAT		./catalogs/FIELDC_hphotom_comb_merge_psfmatch2h.cat
HUDF_CAT	./catalogs/hudf_75Mpc_11_10_136snaps_zyx_FIELDC.txt
SUBHALO_CAT	./catalogs/ZYX_catalogfiles/
CYLINDER_CAT	./catalogs/FIELDC_cylinder_image_header_info/
SNAPSHOT_MIN	02
SNAPSHOT_MAX	95
X_ANGULAR_FOV	0.0008264462
Y_ANGULAR_FOV	0.0008264462
X_CENTER	2048
Y_CENTER	2048
